# Splunk Tables


### Installing
1. Clone the repository
2. sudo npm install
3. bower install
4. grunt serve (for dev version)
5. grunt build (to build)


### ToDos

1. Handle zombie views more elegantly
2. Improve CSS by removing uncessary bootstrap parts
3. Form validation
4. Export to JSON file as well
5. Testing