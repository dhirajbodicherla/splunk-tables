/*global define*/

define([
    'jquery',
    'backbone',
    'views/ColumnsDefinitionView',
    'views/RecordsView',
    'collections/ColumnsDefinitionCollection',
    'collections/RecordsCollection',
    'models/ColumnDefinitionModel',
], function ($, Backbone, ColumnsDefinitionView,
            RecordsView, columnsDefinitionCollection, RecordsCollection,
            ColumnsDefinitionModel) {
    'use strict';

    var IndexRouter = Backbone.Router.extend({
        routes: {
          "" : "index",
          "main": "main"
        },

        initialize: function(){
          this.mainContainer = $('#mainContainer');
        },

        index: function(){
          if(this.columnsDefinitionView){
            this.columnsDefinitionView.trigger('destroy');
          }
          this.columnsDefinitionView = new ColumnsDefinitionView({
            el: this.mainContainer
          }).render();

        },

        main: function(){
          if(this.recordsView){
            this.recordsView.trigger('destroy');
          }

          this.recordsView = new RecordsView({
            el: this.mainContainer
          }).render();
        }

    });

    return IndexRouter;
});
