/*global define*/

define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    'use strict';

    var RecordModel = Backbone.Model.extend({
        url: '',

        initialize: function() {
        },

        defaults: {
        },

        validate: function(attrs, options) {
        },

        parse: function(response, options)  {
            return response;
        },

        getJSON: function(){
            var attrs = this.toJSON();
            delete attrs.id;
            return attrs;
        }
    });

    return RecordModel;
});
