/*global define*/

define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    'use strict';

    var ColumnDefinitionModel = Backbone.Model.extend({
        url: '',

        idAttribute: 'id',

        initialize: function() {
        },

        defaults: {
          fieldName: '',
          isRequired: false
        },

        validate: function(attrs, options) {
        },

        parse: function(response, options)  {
            return response;
        }
    });

    return ColumnDefinitionModel;
});
