/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function ($, _, Backbone, JST) {
    'use strict';

    var ColumnsDefinitionRowView = Backbone.View.extend({
        template: JST['app/scripts/templates/ColumnsDefinitionRowView.ejs'],

        tagName: 'tr',

        className: 'column-definition-row',

        events: {
          "click .deleteRow": "deleteRowClickHandler"
        },

        deleteRowClickHandler: function(){
          var _this = this;
          if(this.model.get('isRequired')){
            alert('This is a mandatory field. It cannot be deleted');
          }else{
            this.model.destroy({
              success: function(){
                _this.$el.remove();
              }
            });
          }
        },

        initialize: function () {
          _.bindAll(this, 'saveModel');
          this.listenTo(this.model, 'save', this.saveModel);
        },

        saveModel: function(){
          var _this = this;
          var fieldName = this.$('input[name="fieldName"]').val().trim();
          _this.model.save({
            "fieldName": fieldName
          });
        },

        render: function () {
          this.$el.html(this.template(this.model.toJSON()));
          return this.$el;
        }
    });

    return ColumnsDefinitionRowView;
});
