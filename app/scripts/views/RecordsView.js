/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'models/RecordModel',
    'views/RecordsView',
    'views/RecordsRowView',
    'collections/ColumnsDefinitionCollection',
    'collections/RecordsCollection'
], function ($, _, Backbone, JST, RecordModel, RecordsView, RecordsRowView, columnsDefinitionCollection, RecordsCollection) {
    'use strict';

    var RecordsDisplayView = Backbone.View.extend({
        template: JST['app/scripts/templates/RecordsView.ejs'],
        columnTemplate: _.template('<th><%=fieldName%></th>'),

        tagName: 'div',

        events: {
          "click #backToColDefs" : "backToColDefsClickHandler",
          "click #addRow": "addRowClickHandler",
          "click #exportToCSV": "exportToCSVClickHandler"
        },

        backToColDefsClickHandler: function(e){
          e.preventDefault();
          Backbone.history.navigate('/', {"trigger": true});
        },

        initialize: function (options) {
          var _this = this;
          _.bindAll(this, 'addRecordRow', 'backToColDefsClickHandler', 'addRowClickHandler', 'exportToCSVClickHandler', 'renderRecordRows', 'renderRecordColumns', 'destroy');
          this.listenTo(this, 'destroy', this.destroy);
          this.columnsDefinitionCollection = columnsDefinitionCollection;
          this.recordsCollection = new RecordsCollection();
        },

        render: function () {
          var _this = this;
          this.$el.html(this.template());
          this.columns = [];
          this.columnsDefinitionCollection.fetch({
            success: function(data){
              _this.columns = data.toJSON();
              _this.renderRecordColumns();
            }
          });
          this.recordsCollection = new RecordsCollection();
          this.recordsCollection.fetch({
            success: function(){
              _this.renderRecordRows();
            }
          });
          return this;
        },

        renderRecordColumns: function(){
          var colTemplates = ['<tr>'];
          for(var i=0;i<this.columns.length; i++){
            colTemplates.push( this.columnTemplate({fieldName: this.columns[i].fieldName}) );
          }
          colTemplates.push( this.columnTemplate({fieldName: 'Actions'}), '</tr>' );
          this.$('table thead').html(colTemplates.join(''));
        },

        renderRecordRows: function(){
          this.$('table tbody').empty();
          this.recordsCollection.forEach(this.addRecordRow);
        },

        addRowClickHandler: function(){
          var modelAttrs = {};
          var columns = this.columns;
          for(var i=0;i<this.columns.length;i++){
            modelAttrs[this.columns[i].fieldName] = "";
          }
          var recordModel = new RecordModel(modelAttrs);
          this.recordsCollection.add(recordModel, {silent: true});
          this.addRecordRow(recordModel);
          this.$('table tr').last().find('.record-input').first().focus();
        },

        exportToCSVClickHandler: function(e){
          e.preventDefault();
          var columns = this.columns.map(function(v, i){ return v.id });
          var tableHeading = this.columns.map(function(v, i){ return v.fieldName });
          var tableContent = this.recordsCollection.toJSON();
          var output = '';
          tableContent = tableContent.map(function(v,i){
            var output = [];
            for(var key in v){
              if(columns.indexOf(key) !== -1){
                output.push(v[key]);
              }
            }
            return output.join(',');
          });
          tableHeading = tableHeading.join(',') + '\n',
          tableContent = tableContent.join('\n');
          output = tableHeading + tableContent;
          var data = 'data:application/csv;charset=UTF-8,' + encodeURIComponent(output);
          var evt = document.createEvent("HTMLEvents");
          evt.initEvent("click");
          var fakeAnchor = $('<a></a>').attr('href', data).attr('download', "data.csv");
          fakeAnchor.get(0).dispatchEvent(evt);
        },

        addRecordRow: function(recordModel){
          var recordsRowView = new RecordsRowView({
            columns: this.columns,
            model: recordModel
          });
          this.$('table').append(recordsRowView.render());
        },

        destroy: function(){
            this.unbind();
            this.undelegateEvents();
            this.$el.empty();
        }
    });

    return RecordsDisplayView;
});
