/*global define*/

define([
  'jquery',
  'underscore',
  'backbone',
  'templates'
], function ($, _, Backbone, JST) {
  'use strict';

  var RecordRowView = Backbone.View.extend({
    template: JST['app/scripts/templates/RecordsRowView.ejs'],

    tagName: 'tr',

    className: 'record-row',

    events: {
      "click .editRow": "editRowClickHandler",
      "click .saveRow": "saveRowClickHandler",
      "click .deleteRow": "deleteRowClickHandler"
    },

    initialize: function(options){
      if(!this.model.id)
        this.$el.addClass('edit');
      this.columns = options.columns;
      _.bindAll(this, 'saveRowClickHandler');
    },

    editRowClickHandler: function(){
      this.$el.addClass('edit');
    },

    saveRowClickHandler: function(){
      var _this = this;
      var dataToSave = {};
      this.$('input[type="text"]').each(function(){
        dataToSave[$(this).attr('name')] = $(this).val();
      });

      var uniqueIDID = this.columns.filter(function(item){
        return item.fieldName == "ID";
      })[0].id;

      var sameIDChecker = this.model.collection.filter(function(recordModel){
        return recordModel.get(uniqueIDID) === dataToSave[uniqueIDID] && recordModel.id !== _this.model.id;
      }).length;

      if(!sameIDChecker){
        this.model.save(dataToSave, {
          success: function(){
            _this.$el.removeClass('edit');
            _this.render();
          }
        });
      }else{
        alert('Another entry with same ID value found. ID is unique.');
      }
    },

    deleteRowClickHandler: function(){
      var _this = this;
      this.model.destroy({
        success: function(){
          _this.$el.remove();
        }
      });
    },

    render: function () {
      var rowValues = [];
      var modelAttrs = this.model.getJSON();
      var numberOfColumns = this.columns.map(function(v,i){ return v.fieldName; }).length;

      for(var i=0;i<numberOfColumns;i++){
        var fieldName = this.columns[i].fieldName;
        var fieldID = this.columns[i].id;
        var row = {};
        row['name'] = modelAttrs[fieldID] || '';
        row['id'] = fieldID;
        rowValues.push(row);
      }

      this.$el.html(this.template({
        attrs: rowValues
      }));
      return this.$el;
    }
  });

  return RecordRowView;
});
