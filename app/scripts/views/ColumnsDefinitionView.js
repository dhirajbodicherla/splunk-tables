/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'views/ColumnsDefinitionRowView',
    'models/ColumnDefinitionModel',
    'collections/ColumnsDefinitionCollection'
], function ($, _, Backbone, JST, ColumnsDefinitionRowView, ColumnDefinitionModel, columnsDefinitionCollection) {
    'use strict';

    var ColumnsDefinitionView = Backbone.View.extend({
        template: JST['app/scripts/templates/ColumnsDefinitionView.ejs'],

        tagName: 'div',

        id: '',

        className: '',

        events: {
            "click #addColumn": "addColumnClickHandler",
            "click #saveFields": "saveFieldsClickHandler"
        },

        addColumnClickHandler: function(){
          var columnDefinitionModel = new ColumnDefinitionModel();
          this.collection.add(columnDefinitionModel, {silent: true});
          this.addColumnsDefinitionRowView(columnDefinitionModel);
          this.$('.column-definition-input').last().focus();
        },

        saveFieldsClickHandler: function(){
          this.collection.models.forEach(function(model){
            model.trigger('save');
          });
          Backbone.history.navigate('/main', {trigger: true});
        },

        initialize: function () {
          _.bindAll(this, 'renderAll', 'addColumnsDefinitionRowView', 'saveFieldsClickHandler', 'destroy');
          this.listenTo(this, 'destroy', this.destroy);
          this.collection = columnsDefinitionCollection;
        },

        render: function () {
          this.$el.html(this.template());
          this.renderAll();
          return this;
        },

        renderAll: function(){
          this.$('table tbody').empty();
          this.collection.forEach(this.addColumnsDefinitionRowView);
        },

        addColumnsDefinitionRowView: function(columnsDefinitionModel){
          var columnsDefinitionRowView = new ColumnsDefinitionRowView({model: columnsDefinitionModel});
          this.$('table tbody').append(columnsDefinitionRowView.render());
        },

        destroy: function(){
          this.unbind();
          this.undelegateEvents();
          this.$el.empty();
        }

    });

    return ColumnsDefinitionView;
});
