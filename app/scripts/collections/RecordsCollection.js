/*global define*/

define([
    'underscore',
    'backbone',
    'localStorage',
    'models/RecordModel'
], function (_, Backbone, localStorage, RecordModel) {
    'use strict';

    var RecordCollectionCollection = Backbone.Collection.extend({
        model: RecordModel,
        localStorage: new localStorage("rows-collection")
    });

    return RecordCollectionCollection;
});
