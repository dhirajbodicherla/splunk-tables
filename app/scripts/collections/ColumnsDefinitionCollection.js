/*global define*/

define([
    'underscore',
    'backbone',
    'localStorage',
    'models/ColumnDefinitionModel',
], function (_, Backbone, localStorage, ColumnDefinitionModel) {
    'use strict';

    var ColumnsDefinitionCollection = Backbone.Collection.extend({
        model: ColumnDefinitionModel,
        localStorage: new localStorage("column-def-collection")
    });

    var columnsDefinitionCollection = new ColumnsDefinitionCollection();
    columnsDefinitionCollection.fetch().done(function(d){
        if(d.length == 0){
            columnsDefinitionCollection.create({
                "fieldName": "ID",
                "isRequired": true
            });
        }
    });



    return columnsDefinitionCollection;
});
